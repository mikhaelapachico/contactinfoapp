module.exports = mongoose => {
    const UserInfo = mongoose.model(
      "UserInfo",
      mongoose.Schema(
        {
          FirstName: String,
          LastName: String,
          DeliveryAddress: String,
          BillingAddress: String
        },
        { timestamps: true }
      )
    );
  
    return UserInfo;
  };