const dbConfig = require("../Config/db.config.js");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.UserInfo = require("./UserInfo.model.js")(mongoose);

module.exports = db;