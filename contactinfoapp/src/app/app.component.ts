import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { UserInfoService } from './Service/user-info-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'contactinfoapp';

  UserInfo: FormGroup;
  UserInfoService: UserInfoService;

  constructor(private userInfoService: UserInfoService) {
    this.UserInfo = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      deliveryAddress: new FormControl(''),
      billingAddress: new FormControl('')
    });

    this.UserInfoService = userInfoService;
  }

  ngOninit() {
    this.GetUserInfo();
  }

  public SaveUserInfo() {
    alert('Hello');
  }

  public GetUserInfo() {

  }
}
